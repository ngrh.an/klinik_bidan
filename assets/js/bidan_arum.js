$(document).ready(function() {

    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    }),
    $('#datepicker1').datepicker({
        format: "yyyy-mm",
        autoclose: true,
        viewMode: "months", 
        minViewMode: "months"
    }),
	// hide ketika page dibuka
	$('#datepicker1').prop('disabled', true);

    /**
     * PASIEN
     */
    var table = $('#table').DataTable({ 

        // "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "scrollX": true,
        // "pageLength": 50,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + "pasien/ajax_list",
            "type": "POST",
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable,
        }
        ],
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data",
            "sInfo": "Tampilkan _START_ - _END_ data dari _TOTAL_ data",
            "sInfoFiltered": "",
            "sSearch": "Cari Pasien",
        },

    });

    // send data
    $('#table tbody').on( 'click', 'tr', function () {
        var data = table.row( this ).data();
        window.location.href = base_url + 'pasien/detail/' + data[7];
    });

    /**
     * LAPORAN UMUM
     */
    var umum = $('#tableUmum').DataTable({ 

        // "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "scrollX": true,
        // "pageLength": 50,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + "laporan/ajax_umum",
            "type": "POST",
            "data": function ( data ) {
                data.hari = $('#datepicker').val();
                data.bulan = $('#datepicker1').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data",
            "sInfo": "Tampilkan _START_ - _END_ data dari _TOTAL_ data",
            "sInfoFiltered": "",
            "sSearch": "Cari Pasien",
        },

    });

    // send data
    $('#tableUmum tbody').on( 'click', 'tr', function () {
        var data = umum.row( this ).data();
        window.location.href = base_url + 'laporan/umum/detail/' + data[7];
    });

    $('#btn-filter').click(function(){ //button filter event click
        umum.ajax.reload();  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        umum.ajax.reload();  //just reload table
    });

    /**
     * LAPORAN BIDAN
     */
    var bidan = $('#tableBidan').DataTable({ 

        // "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "scrollX": true,
        // "pageLength": 50,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + "laporan/ajax_bidan",
            "type": "POST",
            "data": function ( data ) {
                data.hari = $('#datepicker').val();
                data.bulan = $('#datepicker1').val();
                data.kategori = $('#kategori').val();
            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            { 
                "targets": [ 0 ], //first column / numbering column
                "orderable": false, //set not orderable
            },
            {
                // set width on columns
                render: function (data) {
                    return "<div class='text-wrap' style='width: 250px;'>" + data + "</div>";
                },
                targets: 5
            },
            {
                // set width on columns
                render: function (data) {
                    return "<div class='text-wrap' style='width: 250px;'>" + data + "</div>";
                },
                targets: 6
            },
        ],
        "oLanguage": {
            "sLengthMenu": "Tampilkan _MENU_ data",
            "sInfo": "Tampilkan _START_ - _END_ data dari _TOTAL_ data",
            "sInfoFiltered": "",
            "sSearch": "Cari Pasien",
        },

    });

    // send data
    $('#tableBidan tbody').on( 'click', 'tr', function () {
        var data = bidan.row( this ).data();
        window.location.href = base_url + 'laporan/bidan/detail/' + data[8];
    });

    $('#btn-filter').click(function(){ //button filter event click
        bidan.ajax.reload();  //just reload table
    });
    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        bidan.ajax.reload();  //just reload table
    });

    // hide bulanan
    $('#harian').on('click', function() {
        $('#datepicker1').prop('disabled', true);
        $('#datepicker').prop('disabled', false);

        $('#form-filter')[0].reset();
        umum.ajax.reload();  //just reload table

        $('.panel-body h4').html('Tampilkan Laporan Harian');
    }),
    // hide harian
    $('#bulanan').on('click', function() {
        $('#datepicker').prop('disabled', true);
        $('#datepicker1').prop('disabled', false);

        $('#form-filter')[0].reset();
        umum.ajax.reload();  //just reload table

        $('.panel-body h4').html('Tampilkan Laporan Bulanan');
    });
});