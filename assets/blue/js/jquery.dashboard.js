/**
* Theme: Moltran Admin Template
* Author: Coderthemes
* Module/App: Dashboard
*/


!function($) {
    "use strict";

    var Dashboard = function() {
        this.$body = $("body")
        this.$realData = []
    };

    //creates plot graph
    Dashboard.prototype.createPlotGraph = function(selector, data1, data2, labels, colors, borderColor, bgColor) {
      //shows tooltip
      function showTooltip(x, y, contents) {
        $('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
          position: 'absolute',
          top: y + 5,
          left: x + 5
        }).appendTo("body").fadeIn(200);
      }

      $.plot($(selector),
        [ { data: data1,
            label: labels[0],
            color: colors[0]
          },
          { data: data2,
            label: labels[1],
            color: colors[1]
          }
        ],
        {
          series: {
            lines: {
            show: true,
            fill: true,
            lineWidth: 1,
            fillColor: {
              colors: [ { opacity: 0.0 },
                        { opacity: 0.7 }
                      ]
            }
          },
          points: {
            show: true
          },
          shadowSize: 0
          },
          legend: {
          position: 'nw'
        },
        grid: {
          hoverable: true,
          clickable: true,
          borderColor: borderColor,
          borderWidth: 0,
          labelMargin: 10,
          backgroundColor: bgColor
        },
        yaxis: {
          // min: 0,
          // max: 20,
          color: 'rgba(0,0,0,0)'
        },
        xaxis: {
          color: 'rgba(0,0,0,0)'
        },
        tooltip: true,
        tooltipOpts: {
            content: '%s: Value of %x is %y',
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        }
      });
    },
    //end plot graph

    //initializing various charts and components
    Dashboard.prototype.init = function() {
      //plot graph data
      var umum = [
          [0, 3],
          [1, 5],
          [2, 2],
          [3, 8],
          [4, 5]
      ];
      var bidan = [
          [0, 5],
          [1, 4],
          [2, 6],
          [3, 6],
          [4, 0]
      ];
      var plabels = ["Umum", "Bidan"];
      var pcolors = ['#51abff', '#006ba2'];
      var borderColor = '#fff';
      var bgColor = '#fff';
      this.createPlotGraph("#website-stats", umum, bidan, plabels, pcolors, borderColor, bgColor);

    },

    //init Dashboard
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
    
}(window.jQuery),

//initializing Dashboard
function($) {
    "use strict";
    $.Dashboard.init()
}(window.jQuery);


