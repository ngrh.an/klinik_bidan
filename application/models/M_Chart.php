<?php

class M_Chart extends CI_Model {

	public function getChartData($tahun)
	{
		// codeigniter doesn't support union
		$query = $this->db->query("select tgl_periksa, count(tgl_periksa) as jumlah, 'umum' as status from periksa_umum group by MONTH(tgl_periksa) union select tgl_periksa, count(tgl_periksa) as jumlah, 'bidan' as status from periksa_bidan group by MONTH(tgl_periksa)");

		return $query->result();
		// var_dump($query->result()); die();
	}

}