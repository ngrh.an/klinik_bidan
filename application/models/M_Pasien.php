<?php

class M_Pasien extends CI_Model {

	// data table
	var $table = 'pasien';
	
	var $column_order = array(null, 'nama','alamat','phone','tgl_lahir','jenis_kelamin','pekerjaan', 'pendidikan', 'agama'); //set column field database for datatable orderable
	var $column_search = array('id', 'nama','alamat','phone','tgl_lahir','jenis_kelamin','pekerjaan', 'pendidikan', 'agama'); //set column field database for datatable searchable 
	var $order = array('id' => 'asc'); // default order

	private function _get_datatables_query()
	{
		$this->db->from($this->table);
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function getPasien($id)
	{
		$query = $this->db->get_where('pasien', ['id' => $id]);
		return $query->result();
	}

	public function insertPasien()
	{
		// input umur -> convert ke tanggal
		if ( empty($this->input->post('tanggal')) ) {
			// cek umur dalam bulan
			if ( $this->input->post('bulan') == 'bulan' ) {
				$tanggal = $this->umurBulan($this->input->post('umur'));
			} else {
				$tanggal = $this->addUmur($this->input->post('umur'));
			}
		} else {
			$tanggal = date_format(date_create($this->input->post('tanggal')), 'Y-m-d');
		}

		$data = array(
			'id' => null,
			'nama' => $this->input->post('nama'),
			'alamat' => $this->input->post('alamat'),
			'phone' => $this->input->post('phone'),
			'tgl_lahir' => $tanggal,
			'jenis_kelamin' => $this->input->post('gender'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'pendidikan' => $this->input->post('pendidikan'),
			'agama' => $this->input->post('agama')
		);

		// var_dump($data); die();
		$this->db->insert('pasien', $data);
	}

	public function updatePasien($id)
	{
		// input umur -> convert ke tanggal
		if ( empty($this->input->post('tanggal')) ) {
			$tanggal = $this->addUmur($this->input->post('umur'));
		} else {
			$tanggal = date_format(date_create($this->input->post('tanggal')), 'Y-m-d');
		}
		
		$data = array(
			'id' => $this->input->post('id'),
			'nama' => $this->input->post('nama'),
			'alamat' => $this->input->post('alamat'),
			'phone' => $this->input->post('phone'),
			'tgl_lahir' => $tanggal,
			'jenis_kelamin' => $this->input->post('gender'),
			'pekerjaan' => $this->input->post('pekerjaan'),
			'pendidikan' => $this->input->post('pendidikan'),
			'agama' => $this->input->post('agama')
		);

		$this->db->where('id', $id);
		$this->db->update('pasien', $data);
	}

	public function hapusPasien($id)
	{
		$query = $this->db->delete('pasien', array('id' => $id));
		return $query;
	}

	// menghitung tgl lahir berdasarkan umur tahun
	public function addUmur($umur)
	{
		$umur = $umur;
		$y = date('Y');

		$tgl_lahir = ($y - $umur) . '-' . date('m') . '-' . date('d');
		return $tgl_lahir;
	}

	// menghitung umur dengan tgl lahir berdasarkan tahun dan bulan
	// output tahun
	public function hitungUmurTahun($birthday)
	{
		// date in yyyy-mm-dd format
		// explode the date to get month, day and year
		$birthday = explode("-", $birthday);
		$bd = date("md", date("U", mktime(0, 0, 0, $birthday[1], $birthday[2], $birthday[0])));

		// get date from date or birthday
		if ( date('md') >= $bd ) {
			$umur = (date("Y") - $birthday[0]);
		} else {
			$umur = (date("Y") - $birthday[0]) - 1;
		}

		return $umur;
	}

	// menghitung umur dengan tgl lahir berdasarkan bulan
	// output bulan
	public function hitungUmurBulan($birthday)
	{
		$birthday = new DateTime($birthday);
		$diff = $birthday->diff(new DateTime());
		$months = $diff->format('%m') + 12 * $diff->format('%y');

		return $months;
	}

	// cek umur tahun apa bulan
	public function hitungUmur($date)
	{
		if ( $this->hitungUmurTahun($date) > 0 ) {
			$umur = $this->hitungUmurTahun($date) . ' Tahun';
		} else {
			$umur = $this->hitungUmurBulan($date) . ' Bulan';
		}

		return $umur;
	}

	// menghitung tgl lahir berdasarkan umur bulan
	public function umurBulan($bulan)
	{
		$date = date('Y-m-d');
		$months = date('Y-m-d', strtotime('-' . $bulan . ' months', strtotime($date)));

		return $months;
	}

}