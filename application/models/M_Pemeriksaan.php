<?php

class M_pemeriksaan extends CI_Model {

	public function insertUmum()
	{
		$time = $this->dateTime();
		$tanggal = date_format(date_create($this->input->post('tanggal')), 'Y-m-d');

		$data = array(
			'id' => null,
			'id_pasien' => $this->input->post('id_pasien'),
			'nama' => $this->input->post('nama'),
			'umur' => $this->input->post('umur'),
			'alamat' => $this->input->post('alamat'),
			'tgl_periksa' => $tanggal . $time->format(' H:i:s'),
			'subjective' => $this->input->post('subjective'),
			'objective' => $this->input->post('objective'),
			'diagnosa' => $this->input->post('diagnosa'),
			'terapi' => $this->input->post('terapi')
		);
		// var_dump($data); die();
		$this->db->insert('periksa_umum', $data);
	}

	public function insertBidan()
	{
		$time = $this->dateTime();
		$tanggal = date_format(date_create($this->input->post('tanggal')), 'Y-m-d');

		$data = array(
			'id' => null,
			'id_pasien' => $this->input->post('id_pasien'),
			'nama' => $this->input->post('nama'),
			'umur' => $this->input->post('umur'),
			'alamat' => $this->input->post('alamat'),
			'tgl_periksa' => $tanggal . $time->format(' H:i:s'),
			'subjective' => $this->input->post('subjective'),
			'objective' => $this->input->post('objective'),
			'diagnosa' => $this->input->post('diagnosa'),
			'terapi' => $this->input->post('terapi'),
			'keterangan' => $this->input->post('keterangan'),
			'laporan' => $this->input->post('laporan')
		);

		// var_dump($data); die();
		$this->db->insert('periksa_bidan', $data);
	}

	public function getDataPemeriksaanUmum($id)
	{
		$query = $this->db->get_where('periksa_umum', ['id' => $id]);
		return $query->result();
	}

	public function getDataPemeriksaanBidan($id)
	{
		$query = $this->db->get_where('periksa_bidan', ['id' => $id]);
		return $query->result();
	}

	public function updatePemeriksaanUmum($id)
	{
		$data = array(
			'id_pasien' => $this->input->post('id_pasien'),
			'subjective' => $this->input->post('subjective'),
			'objective' => $this->input->post('objective'),
			'diagnosa' => $this->input->post('diagnosa'),
			'terapi' => $this->input->post('terapi')
		);

		$this->db->where('id', $id);
		$this->db->update('periksa_umum', $data);
	}

	public function updatePemeriksaanBidan($id)
	{
		$data = array(
			'id_pasien' => $this->input->post('id_pasien'),
			'subjective' => $this->input->post('subjective'),
			'objective' => $this->input->post('objective'),
			'diagnosa' => $this->input->post('diagnosa'),
			'terapi' => $this->input->post('terapi'),
			'keterangan' => $this->input->post('keterangan'),
			'laporan' => $this->input->post('laporan')
		);

		$this->db->where('id', $id);
		$this->db->update('periksa_bidan', $data);
	}

	public function hapusPemeriksaanUmum($id)
	{
		$query = $this->db->delete('periksa_umum', array('id' => $id));
		return $query;
	}

	public function hapusPemeriksaanBidan($id)
	{
		$query = $this->db->delete('periksa_bidan', array('id' => $id));
		return $query;
	}

	// correct tanggal sekarang
	public function dateTime()
	{
		$date = new DateTime("now", new DateTimeZone('Asia/Jakarta'));
		// $date->format('Y-m-d h:i:s');
		return $date;
	}

}