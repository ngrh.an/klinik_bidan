<?php

class M_Laporan extends CI_Model {

	// data table
	var $table_umum = 'periksa_umum';
	var $table_bidan = 'periksa_bidan';
	
	var $column_order_umum = array(null, 'nama', 'umur', 'alamat', 'tgl_periksa', 'diagnosa', 'terapi'); //set column field database for datatable orderable
	var $column_order_bidan = array(null, 'nama', 'umur', 'alamat', 'tgl_periksa', 'diagnosa', 'terapi', 'laporan'); //set column field database for datatable orderable
	var $column_search_umum = array('id', 'nama', 'umur', 'alamat', 'tgl_periksa', 'diagnosa', 'terapi'); //set column field database for datatable searchable 
	var $column_search_bidan = array('id', 'nama', 'umur', 'alamat', 'tgl_periksa', 'diagnosa', 'terapi', 'laporan'); //set column field database for datatable searchable 
	var $order_umum = array('id' => 'asc'); // default order
	var $order_bidan = array('id' => 'asc'); // default order

	// umum
	private function _get_datatables_query()
	{
		//add custom filter here
		if ($this->input->post('hari'))
		{
			$this->db->where('tgl_periksa', $this->input->post('hari'));
		}
		if ($this->input->post('bulan'))
		{
			$this->db->like('tgl_periksa', $this->input->post('bulan'));
		}

		$this->db->from($this->table_umum);
		$i = 0;
	
		foreach ($this->column_search_umum as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_umum) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_umum[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_umum))
		{
			$order_umum = $this->order_umum;
			$this->db->order_by(key($order_umum), $order_umum[key($order_umum)]);
		}
	}

	public function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table_umum);
		return $this->db->count_all_results();
	}

	// bidan
	private function _get_datatables_query_bidan()
	{
		//add custom filter here
		if ($this->input->post('hari'))
		{
			$this->db->where('tgl_periksa', $this->input->post('hari'));
		}
		if ($this->input->post('bulan'))
		{
			$this->db->like('tgl_periksa', $this->input->post('bulan'));
		}
		if ($this->input->post('kategori'))
		{
			$this->db->where('laporan', $this->input->post('kategori'));
		}

		$this->db->from($this->table_bidan);
		$i = 0;
	
		foreach ($this->column_search_bidan as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_bidan) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_bidan[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_bidan))
		{
			$order_bidan = $this->order_bidan;
			$this->db->order_by(key($order_bidan), $order_bidan[key($order_bidan)]);
		}
	}

	public function get_datatables_bidan()
	{
		$this->_get_datatables_query_bidan();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered_bidan()
	{
		$this->_get_datatables_query_bidan();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_bidan()
	{
		$this->db->from($this->table_bidan);
		return $this->db->count_all_results();
	}

	public function getDataPemeriksaan($id) 
	{
		if ($this->uri->segment('2') == 'umum') {
			$query = $this->db->get_where('periksa_umum', ['id' => $id]);
		}
		if ($this->uri->segment('2') == 'bidan') {
			$query = $this->db->get_where('periksa_bidan', ['id' => $id]);
		}
		return $query->result();
	}

	// history
	public function historyPemeriksaan($id)
	{
		$query = $this->db->query("select *, 'bidan' as status from periksa_bidan where id_pasien = " . $id . " union select *, '' as keterangan, '' as laporan, 'umum' as status from periksa_umum where id_pasien = " . $id . " order by tgl_periksa desc limit 5");

		return $query->result();
	}

	// history bidan
	public function historyBidan($id)
	{
		$query = $this->db->order_by('tgl_periksa', 'DESC')->get_where('periksa_bidan', ['id_pasien' => $id], 5);
		return $query->result();
	}

}