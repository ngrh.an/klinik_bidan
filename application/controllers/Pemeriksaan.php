<?php

class Pemeriksaan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Pasien');
		$this->load->model('M_Pemeriksaan');
	}

	// pendaftaran pemeriksaan umum
	public function umum($id)
	{
		$data['title'] = 'Menu Pemeriksaan Umum';

		// load data pasien
		$data['pemeriksaan'] = $this->M_Pasien->getPasien($id);
		$data['umur'] = $this->M_Pasien->hitungUmur($data['pemeriksaan'][0]->tgl_lahir);
		$data['tgl_periksa'] = $this->M_Pemeriksaan->dateTime();
		// var_dump($data['umur']); die();
		$this->load->view('pemeriksaan/umum', $data);
		
		// input pemeriksaan
		if ( $this->input->post('simpan') ) {
			$this->M_Pemeriksaan->insertUmum();
			redirect('laporan/umum');
		}
	}

	// pendaftaran pemeriksaan bidan
	public function bidan($id)
	{
		$data['title'] = 'Menu Pemeriksaan Bidan';

		// load data pasien
		$data['pemeriksaan'] = $this->M_Pasien->getPasien($id);
		$data['umur'] = $this->M_Pasien->hitungUmur($data['pemeriksaan'][0]->tgl_lahir);
		$data['tgl_periksa'] = $this->M_Pemeriksaan->dateTime();
		$this->load->view('pemeriksaan/bidan', $data);
		
		// input pemeriksaan
		if ( $this->input->post('laporan') ) {
			$this->M_Pemeriksaan->insertBidan();
			redirect('laporan/bidan');
		}
	}

	// pendaftaran pemeriksaan pasien lama
	public function pemeriksaanLama($id)
	{
		$this->form_validation->set_rules('tanggal', 'tanggal periksa', array('required', 'regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}/]'));
		$data['pemeriksaan'] = $this->M_Pasien->getPasien($id);
		$data['umur'] = $this->M_Pasien->hitungUmur($data['pemeriksaan'][0]->tgl_lahir);

		// cek jika pasien umum
		if ( $this->uri->segment('3') == 'umum' ) {

			$data['title'] = 'Input Data Pemeriksaan Umum';

			// validate form untuk tgl pemeriksaan
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('pasien-lama/pemeriksaan/umum', $data);
			} else {
				$this->M_Pemeriksaan->insertUmum();
				redirect('laporan/umum');
			}
		}
		// cek jika pasien bidan
		if ( $this->uri->segment('3') == 'bidan' ) {

			$data['title'] = 'Input Data Pemeriksaan Bidan';
			
			// validate form untuk tgl pemeriksaan
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('pasien-lama/pemeriksaan/bidan', $data);
			} else {
				if ( $this->input->post('laporan') ) {
					$this->M_Pemeriksaan->insertBidan();
					redirect('laporan/bidan');
				}
			}
		}
	}

	// edit data pemeriksaan
	public function editPemeriksaan($id)
	{
		// cek jika data pasien umum
		if ($this->uri->segment('2') == 'umum')
		{
			$data['title'] = 'Edit Data Pemeriksaan Umum';
			$data['pemeriksaan'] = $this->M_Pemeriksaan->getDataPemeriksaanUmum($id);
			$this->load->view('pemeriksaan/edit/umum', $data);
			// update
			if ($this->input->post('update'))
			{
				$this->M_Pemeriksaan->updatePemeriksaanUmum($id);
				redirect('laporan/umum');
			}
		}
		// cek jika pasien bidan
		if ($this->uri->segment('2') == 'bidan')
		{
			$data['title'] = 'Edit Data Pemeriksaan Bidan';
			$data['pemeriksaan'] = $this->M_Pemeriksaan->getDataPemeriksaanBidan($id);
			$this->load->view('pemeriksaan/edit/bidan', $data);
			// update
			if ( $this->input->post('laporan') )
			{
				$this->M_Pemeriksaan->updatePemeriksaanBidan($id);
				redirect('laporan/bidan');
			}
		}
	}

	// hapus data pemeriksaan
	public function hapusPemeriksaan($id)
	{
		// cek jika pasien umum
		if ($this->uri->segment('2') == 'umum')
		{
			$this->M_Pemeriksaan->hapusPemeriksaanUmum($id);
			redirect('laporan/umum');
		}
		// cek jika pasien bidan
		if ($this->uri->segment('2') == 'bidan')
		{
			$this->M_Pemeriksaan->hapusPemeriksaanBidan($id);
			redirect('laporan/bidan');
		}
	}

}