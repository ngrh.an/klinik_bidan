<?php

class Laporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Laporan');
	}

	public function umum()
	{
		$data['title'] = "Laporan Pemeriksaan Umum";

		$this->load->view('laporan/umum', $data);
	}

    public function bidan()
    {
        $data['title'] = "Laporan Pemeriksaan Bidan";

        $this->load->view('laporan/bidan', $data);
    }

	public function ajax_umum()
    {
        $list = $this->M_Laporan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $laporan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $laporan->nama;
            $row[] = $laporan->umur;
            $row[] = character_limiter($laporan->alamat, 25);
            $row[] = date_format(date_create($laporan->tgl_periksa), 'd M Y');
            $row[] = nl2br($laporan->diagnosa);
            $row[] = nl2br($laporan->terapi);
            $row[] = $laporan->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Laporan->count_all(),
            "recordsFiltered" => $this->M_Laporan->count_filtered(),
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }

    public function ajax_bidan()
    {
        $list = $this->M_Laporan->get_datatables_bidan();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $laporan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $laporan->nama;
            $row[] = $laporan->umur;
            $row[] = character_limiter($laporan->alamat, 20);
            $row[] = date_format(date_create($laporan->tgl_periksa), 'd M Y');
            $row[] = nl2br($laporan->diagnosa);
            $row[] = nl2br($laporan->terapi);
            $row[] = strtoupper($laporan->laporan);
            $row[] = $laporan->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Laporan->count_all_bidan(),
            "recordsFiltered" => $this->M_Laporan->count_filtered_bidan(),
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }

    public function detail($id)
    {
        if ($this->uri->segment('2') == 'umum') {
            $data['title'] = 'Data Detail Pemeriksaan Umum';
            $data['pemeriksaan'] = $this->M_Laporan->getDataPemeriksaan($id);

            $this->load->view('laporan/detail_umum', $data);  
        }
        if ($this->uri->segment('2') == 'bidan') {
            $data['title'] = 'Data Detail Pemeriksaan Bidan';
            $data['pemeriksaan'] = $this->M_Laporan->getDataPemeriksaan($id);

            $this->load->view('laporan/detail_bidan', $data);
        }
        // print_r($data['pasien']);
    }

}