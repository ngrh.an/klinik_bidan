<?php

class CetakLaporan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('fpdf_lib');
		$this->load->model('M_Laporan');
	}

	public function index()
	{
		$pdf = new FPDF();
		$pdf->AddPage('L', 'A4', 0);
		$pdf->SetAutoPageBreak(true, 10);
		$pdf->SetFont('Arial', 'B', 14);
		$pdf->Cell(0, 5, 'LAPORAN PEMERIKSAAN', 0, 0, 'C');
		$pdf->ln();
		$pdf->SetFont('Times', '', 12);
		$pdf->Cell(0, 10, 'Jl. Jogja kembali kepadamu', 0, 0, 'C');
		$pdf->ln(15);

		// table
		$pdf->SetFont('Times', 'B', 12);
		$pdf->Cell(20, 10, 'ID', 1, 0, 'C');
		$pdf->Cell(30, 10, 'Nama', 1, 0, 'C');
		$pdf->Cell(50, 10, 'Alamat', 1, 0, 'C');
		$pdf->Cell(20, 10, 'Umur', 1, 0, 'C');
		$pdf->Cell(35, 10, 'Tanggal Periksa', 1, 0, 'C');
		$pdf->Cell(30, 10, 'Subjective', 1, 0, 'C');
		$pdf->Cell(30, 10, 'Objective', 1, 0, 'C');
		$pdf->Cell(30, 10, 'Diagnosa', 1, 0, 'C');
		$pdf->Cell(30, 10, 'Terapi', 1, 0, 'C');
		$pdf->ln();

		// content
		$pdf->SetFont('Times', '', 11);
		$report = $this->db->get('periksa_umum')->result();
		foreach ($report as $laporan) {
			$pdf->Cell(20, 10, $laporan->id, 1, 0, 'C');
			$pdf->Cell(30, 10, $laporan->nama, 1, 0, 'C');
			$pdf->Cell(50, 10, $laporan->alamat, 1, 0, 'C');
			$pdf->Cell(20, 10, $laporan->umur, 1, 0, 'C');
			$pdf->Cell(35, 10, date_format(date_create($laporan->tgl_periksa), 'd-m-Y'), 1, 0, 'C');
			$pdf->Cell(30, 10, 'Subjective', 1, 0, 'C');
			$pdf->Cell(30, 10, 'Objective', 1, 0, 'C');
			$pdf->Cell(30, 10, $laporan->diagnosa, 1, 0, 'C');
			$pdf->Cell(30, 10, $laporan->terapi, 1, 0, 'C');
			$pdf->ln();
		}

		// output
		$pdf->Output('Documentc.pdf', 'I');
	}

}