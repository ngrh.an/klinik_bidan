<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['M_Pasien', 'M_Laporan']);
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	// view dashboard
	public function dashboard()
	{
		$data['title'] = 'Dashboard';
		$data['headerChart'] = 'Grafik Jumlah Pemeriksaan';
		$data['pasien'] = $this->M_Pasien->count_all();
		$data['umum'] = $this->M_Laporan->count_all();
		$data['bidan'] = $this->M_Laporan->count_all_bidan();
		$this->load->view('dashboard', $data);
	}

}
