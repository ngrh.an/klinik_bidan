<?php

class Pasien extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Pasien');
		$this->load->model('M_Laporan');
	}

	// load list data pasien
    public function index()
    {
        $data['title'] = "Daftar Pasien Lama";
        $this->load->view('pasien/daftar_pasien', $data);
    }

    // load data pasien dengan ajax
	public function ajax_list()
    {
    	// panggil data dari model
        $list = $this->M_Pasien->get_datatables();
        // inisialisasi array kosong
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pasien) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $pasien->nama;
            $row[] = character_limiter($pasien->alamat, 25);
            $row[] = $pasien->phone;
            $row[] = date_format(date_create($pasien->tgl_lahir), 'd M Y');
            $row[] = $pasien->jenis_kelamin;
            $row[] = $pasien->pekerjaan;
            $row[] = $pasien->id;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_Pasien->count_all(),
            "recordsFiltered" => $this->M_Pasien->count_filtered(),
            "data" => $data,
        );
        
        //output to json format
        echo json_encode($output);
    }

    // load data pasien with id
	public function detail($id)
	{
		$data['title'] = 'Data Detail Pasien';
		$data['pasien'] = $this->M_Pasien->getPasien($id);
		$data['umur'] = $this->M_Pasien->hitungUmur($data['pasien'][0]->tgl_lahir);

		// history pemeriksaan
		$data['history'] = $this->M_Laporan->historyPemeriksaan($id);
		$data['historyBidan'] = $this->M_Laporan->historyBidan($id);
		
		$this->load->view('pasien/detail_pasien', $data);
		// print_r($data['pasien']);
	}

	// tambah data pasien
	public function tambah()
	{
		$data['title'] = 'Pendaftaran Pasien Baru';
		
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('phone', 'no. telp', array('numeric', 'max_length[13]'));
		$this->form_validation->set_rules('tanggal', 'tanggal lahir', array('regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}/]'));
		$this->form_validation->set_rules('umur', 'umur', 'numeric');
		$this->form_validation->set_rules('gender', 'jenis kelamin', 'required');

		// validate form
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('pasien/pendaftaran', $data);
		} else {
			$this->M_Pasien->insertPasien();
			// get last insert
			$lastid = $this->db->insert_id();
			redirect('pasien/detail/' . $lastid);
		}
	}

	// INPUTAN PASIEN LAMA
	public function pasienLama()
	{
		// validasi
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('phone', 'no. telp', array('numeric', 'max_length[13]'));
		$this->form_validation->set_rules('tanggal', 'tanggal lahir', array('regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}/]'));
		$this->form_validation->set_rules('umur', 'umur', 'numeric');
		$this->form_validation->set_rules('gender', 'jenis kelamin', 'required');

		// cek jika pasien untuk umum
		if ( $this->uri->segment('3') == 'umum' ) {

			$data['title'] = 'Input Data Pasien Umum';

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('pasien-lama/pendaftaran/umum', $data);
			} else {
				$this->M_Pasien->insertPasien();
				// get last insert
				$lastid = $this->db->insert_id();
				redirect('pemeriksaan/lama/umum/' . $lastid);
			}
		}
		// cek jika pasien untuk bidan
		if ( $this->uri->segment('3') == 'bidan' ) {

			$data['title'] = 'Input Data Pasien Bidan';

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('pasien-lama/pendaftaran/bidan', $data);
			} else {
				$this->M_Pasien->insertPasien();
				// get last insert
				$lastid = $this->db->insert_id();
				redirect('pemeriksaan/lama/bidan/' . $lastid);
			}
		}
	}

	// edit pasien
	public function editPasien($id)
	{
		$data['title'] = 'Edit Data Pasien';
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('phone', 'no. telp', array('numeric', 'max_length[13]'));
		// validate tanggal
		$this->form_validation->set_rules('tanggal', 'tanggal lahir', array('regex_match[/(0[1-9]|1[0-9]|2[0-9]|3(0|1))-(0[1-9]|1[0-2])-\d{4}/]'));
		$this->form_validation->set_rules('umur', 'umur', 'numeric');
		$this->form_validation->set_rules('gender', 'jenis kelamin', 'required');
		$data['pasien'] = $this->M_Pasien->getPasien($id);

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('pasien/edit-pasien', $data);
		} 
		else 
		{
			$this->M_Pasien->updatePasien($id);
			redirect('pasien/detail/' . $id);
		}
	}

	// hapus pasien
	public function hapus($id)
	{
		$this->M_Pasien->hapusPasien($id);
		redirect('pasien');
	}

}