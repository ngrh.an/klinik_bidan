<?php

class Chart extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Chart');
	}

	public function chartPemeriksaan($tahun = '')
	{
		// default tahun = tahun sekarang
		$tahun = ($tahun == '' ? date('Y') : $tahun);

		$chart = $this->M_Chart->getChartData($tahun);
		$array = array();
		foreach ($chart as $data) {
			$row = array();
			$row['bulan'] = $this->bulan($data->tgl_periksa);
			$row['jumlah'] = $data->jumlah;
			$row['pemeriksaan'] = $data->status;

			$array[] = $row;
		}
		header('Content-type: application/json');
		echo json_encode($array);
	}

	public function bulan($bulan)
	{
		// pecah date dan time
		$bulan = explode(' ', $bulan);
		// pecah date
		$bulan = explode('-', $bulan[0]);
		$bulan = date('F', mktime(0, 0, 0, $bulan[1], $bulan[2], $bulan[0]));

		return $bulan;
	}

}