<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome/dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['laporan/umum/detail/(:num)'] = 'laporan/detail/$1';
$route['laporan/bidan/detail/(:num)'] = 'laporan/detail/$1';
$route['pasien/lama/umum'] = 'pasien/pasienLama';
$route['pasien/lama/bidan'] = 'pasien/pasienLama';
$route['pemeriksaan/lama/umum/(:num)'] = 'pemeriksaan/pemeriksaanLama/$1';
$route['pemeriksaan/lama/bidan/(:num)'] = 'pemeriksaan/pemeriksaanLama/$1';
// edit
$route['pasien/edit/(:num)'] = 'pasien/editPasien/$1';
$route['pemeriksaan/umum/edit/(:num)'] = 'pemeriksaan/editPemeriksaan/$1';
$route['pemeriksaan/bidan/edit/(:num)'] = 'pemeriksaan/editPemeriksaan/$1';
// hapus
$route['pasien/hapus/(:num)'] = 'pasien/hapus/$1';
$route['pemeriksaan/umum/hapus/(:num)'] = 'pemeriksaan/hapusPemeriksaan/$1';
$route['pemeriksaan/bidan/hapus/(:num)'] = 'pemeriksaan/hapusPemeriksaan/$1';
// pdf report
$route['cetak'] = 'cetaklaporan/index';










