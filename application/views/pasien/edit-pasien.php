<?php $this->load->view('templates/header') ?>
  
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title"><?= $title ?></h1>
					</div>
					<div class="panel-body">
						<form action="" method="post">
						<input type="hidden" name="id" value="<?= $pasien[0]->id ?>">
						  <div class="form-group row">
						    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama" autocomplete="off" value="<?= $pasien[0]->nama ?>">
						      <small class="form-text text-danger"><?= form_error('nama') ?></small>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan alamat" autocomplete="off" value="<?= $pasien[0]->alamat ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="phone" class="col-sm-3 col-form-label">No. Hp</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="phone" id="phone" placeholder="Masukkan nomer telp" autocomplete="off" value="<?= $pasien[0]->phone ?>">
						      <small class="form-text text-danger"><?= form_error('phone') ?></small>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="tanggal" class="col-sm-3 col-form-label">Tanggal Lahir</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="tanggal" id="tanggal" placeholder="dd-mm-yyyy" autocomplete="off" value="<?= date_format(date_create($pasien[0]->tgl_lahir), 'd-m-Y'); ?>">
						      <small class="form-text text-muted">Dikosongin saja jika tidak tahu tanggal lahir.</small>
						      <small class="form-text text-danger"><?= form_error('tanggal') ?></small>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="umur" class="col-sm-3 col-form-label">Umur</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur" autocomplete="off" value="<?= $umur = set_value('umur') == false ? set_value('umur') : '' ; ?>">
						      <small class="form-text text-muted">Umur akan dihitung berdasarkan tanggal sekarang.</small>
						      <small class="form-text text-danger"><?= form_error('umur') ?></small>
						    </div>
						  </div>
						  <fieldset class="form-group">
						    <div class="row">
						      <legend class="col-form-label col-sm-3 pt-0">Jenis Kelamin</legend>
						      <div class="col-sm-9">
						        <div class="form-check">
						          <input class="form-check-input" type="radio" name="gender" id="male" value="Laki-laki" <?= ($pasien[0]->jenis_kelamin == 'Laki-laki' ? 'checked' : '') ?>>
						          <label class="form-check-label" for="male">
						            Laki-laki
						          </label>
						        </div>
						        <div class="form-check">
						          <input class="form-check-input" type="radio" name="gender" id="female" value="Perempuan" <?= ($pasien[0]->jenis_kelamin == 'Perempuan' ? 'checked' : '') ?>>
						          <label class="form-check-label" for="female">
						            Perempuan
						          </label>
						        </div>
						        <small class="form-text text-danger"><?= form_error('gender') ?></small>
						      </div>
						    </div>
						  </fieldset>
						  <div class="form-group row">
						    <label for="pekerjaan" class="col-sm-3 col-form-label">Pekerjaan</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" placeholder="Masukkan pekerjaan" autocomplete="off" value="<?= $pasien[0]->pekerjaan ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="pendidikan" class="col-sm-3 col-form-label">Pendidikan</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="pendidikan" id="pendidikan" placeholder="Masukkan pendidikan" autocomplete="off" value="<?= $pasien[0]->pendidikan ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="agama" class="col-sm-3 col-form-label">Agama</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="agama" id="agama" placeholder="Masukkan agama" autocomplete="off" value="<?= $pasien[0]->agama ?>">
						    </div>
						  </div>
						  <button type="submit" class="btn btn-primary simpan">Update</button>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
    
<?php $this->load->view('templates/footer') ?>