<?php $this->load->view('templates/header') ?>
   
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-8">
								<h1 class="panel-title"><?= $title ?></h1>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div style="overflow-x: auto;">
							<table id="table" class="display nowrap table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>No</th>
						                <th>Nama</th>
						                <th>Alamat</th>
						                <th>No. Telp</th>
						                <th>Tanggal Lahir</th>
						                <th>Jenis Kelamin</th>
						                <th>Pekerjaan</th>
						            </tr>
						        </thead>
						        <tbody>
						        </tbody>
						    </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('templates/footer') ?>