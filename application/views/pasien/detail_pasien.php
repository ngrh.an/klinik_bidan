<?php $this->load->view('templates/header') ?>
	<div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title">History Pemeriksaan</h1>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-3">
								<div class="panel-group">
								    <div class="panel panel-default">
								        <div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" href="#subjective">Subjective</a>
								      		</h4>
								    	</div>
								    <div id="subjective" class="panel-collapse collapse">
							        	<ul class="list-group border">
							        		<?php foreach ($history as $riwayat) : ?>
										  		<li class="list-group-item"><span class="badge"><?= date_format(date_create($riwayat->tgl_periksa), 'd/m/y') . ' | ' . $riwayat->status ?></span> <?= $riwayat->subjective ?></li>
										  	<?php endforeach ?>
										</ul>
								    </div>
								    </div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="panel-group">
								    <div class="panel panel-default">
								        <div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" href="#objective">Objective</a>
								      		</h4>
								    	</div>
								    <div id="objective" class="panel-collapse collapse">
								        <ul class="list-group border">
										  	<?php foreach ($history as $riwayat) : ?>
										  		<li class="list-group-item"><span class="badge"><?= date_format(date_create($riwayat->tgl_periksa), 'd/m/y') . ' | ' . $riwayat->status ?></span> <?= $riwayat->objective ?></li>
										  	<?php endforeach ?>
										</ul>
								    </div>
								    </div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="panel-group">
								    <div class="panel panel-default">
								        <div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" href="#diagnosa">Diagnosa</a>
								      		</h4>
								    	</div>
								    <div id="diagnosa" class="panel-collapse collapse">
								        <ul class="list-group border">
										  	<?php foreach ($history as $riwayat) : ?>
										  		<li class="list-group-item"><span class="badge"><?= date_format(date_create($riwayat->tgl_periksa), 'd/m/y') . ' | ' . $riwayat->status ?></span> <?= $riwayat->diagnosa ?></li>
										  	<?php endforeach ?>
										</ul>
								    </div>
								    </div>
								</div>
							</div>
							<div class="col-lg-3">
								<div class="panel-group">
								    <div class="panel panel-default">
								        <div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" href="#terapi">Terapi</a>
								      		</h4>
								    	</div>
								    <div id="terapi" class="panel-collapse collapse">
								        <ul class="list-group border">
										  	<?php foreach ($history as $riwayat) : ?>
										  		<li class="list-group-item"><span class="badge"><?= date_format(date_create($riwayat->tgl_periksa), 'd/m/y') . ' | ' . $riwayat->status ?></span> <?= $riwayat->terapi ?></li>
										  	<?php endforeach ?>
										</ul>
								    </div>
								    </div>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="panel-group">
								    <div class="panel panel-default">
								        <div class="panel-heading">
								      		<h4 class="panel-title">
								        		<a data-toggle="collapse" href="#keterangan">Keterangan</a>
								      		</h4>
								    	</div>
								    <div id="keterangan" class="panel-collapse collapse">
								        <ul class="list-group border">
										  	<?php foreach ($historyBidan as $riwayat) : ?>
										  		<li class="list-group-item"><span class="badge"><?= date_format(date_create($riwayat->tgl_periksa), 'd/m/y') . ' | Bidan' ?></span> <?= $riwayat->keterangan ?></li>
										  	<?php endforeach ?>
										</ul>
								    </div>
								    </div>
								</div>
							</div>
						</div>
						<small class="form-text text-muted">*) 5 data pemeriksaan terakhir.</small>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h1 class="panel-title"><?= $title ?></h1>
							</div>
							<div class="col-lg-6 text-right">
								<a href="<?= base_url() ?>pasien/edit/<?= $this->uri->segment('3'); ?>" class="btn btn-primary m-r-5"><i class="fa fa-edit"></i> Edit</a>
								<a href="<?= base_url() ?>pasien/hapus/<?= $pasien[0]->id ?>" class="btn btn-danger" onclick="return confirm('Data pasien <?= $pasien[0]->nama ?> akan dihapus secara permanen, apa anda yakin?');"><i class="fa fa-trash"></i> Hapus</a>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<col width="180">
							<tr>
								<th>Nama</th>
								<td><?= $pasien[0]->nama ?></td>
							</tr>
							<tr>
								<th>Alamat</th>
								<td><?= $pasien[0]->alamat ?></td>
							</tr>
							<tr>
								<th>No. Telp</th>
								<td><?= $pasien[0]->phone ?></td>
							</tr>
							<tr>
								<th>Tanggal Lahir</th>
								<td><?= date_format(date_create($pasien[0]->tgl_lahir), 'd-m-Y') ?></td>
							</tr>
							<tr>
								<th>Umur</th>
								<td><?= $umur ?></td>
							</tr>
							<tr>
								<th>Jenis Kelamin</th>
								<td><?= $pasien[0]->jenis_kelamin ?></td>
							</tr>
							<tr>
								<th>Pekerjaan</th>
								<td><?= $pasien[0]->pekerjaan ?></td>
							</tr>
							<tr>
								<th>Pendidikan</th>
								<td><?= $pasien[0]->pendidikan ?></td>
							</tr>
							<tr>
								<th>Agama</th>
								<td><?= $pasien[0]->agama ?></td>
							</tr>
						</table>
						<label>Data di atas akan diperiksa dengan jenis pemeriksaan?</label>
						<div>
							<a href="<?= base_url() ?>pemeriksaan/bidan/<?= $pasien[0]->id ?>" class="btn btn-primary">Bidan</a>
							<a href="<?= base_url() ?>pemeriksaan/umum/<?= $pasien[0]->id ?>" class="btn btn-secondary">Umum</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('templates/footer') ?>