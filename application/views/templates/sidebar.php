<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="<?= base_url() ?>" class="waves-effect"><i class="fa fa-home"></i><span>Beranda</span></a>
                </li>
				<li>
                    <a href="<?= base_url() ?>pasien/tambah" class="waves-effect"><i class="fa fa-user-plus"></i><span>Pendaftaran Pasien Baru</span></a>
                </li>
                <li>
                    <a href="<?= base_url() ?>pasien" class="waves-effect"><i class="fa fa-search"></i><span>Cari Pasien Lama</span></a>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect"><i class="fa fa-bookmark"></i><span>Lihat Laporan</span><span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?= base_url() ?>laporan/umum">Umum</a></li>
                        <li><a href="<?= base_url() ?>laporan/bidan">Bidan</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="#" class="waves-effect"><i class="fa fa-wheelchair"></i><span>Input Pasien Lama</span><span class="pull-right"><i class="md md-add"></i></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?= base_url() ?>pasien/lama/umum">Pemeriksaan Umum</a></li>
                        <li><a href="<?= base_url() ?>pasien/lama/bidan">Pemeriksaan Bidan</a></li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>