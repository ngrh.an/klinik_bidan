    <!-- <footer class="footer text-right">2015 © Moltran.</footer> -->
    </div>
    <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
            var base_url = "<?= base_url(); ?>";
        </script>
        
        <script src="<?php echo base_url('assets/datatables/js/jquery-2.2.3.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('assets/datatables/js/jquery.dataTables.min.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/js/dataTables.bootstrap.min.js')?>"></script>
        <script src="<?php echo base_url('assets/blue/js/waves.js');?>"></script>
        
        <script src="<?php echo base_url('assets/js/Chart.min.js');?>"></script>
        <!-- CUSTOM JS -->
        <script src="<?php echo base_url('assets/blue/js/jquery.app.js');?>"></script>

        <!-- DATEPICKER -->
        <script src="<?php echo base_url('assets/blue/assets/timepicker/bootstrap-datepicker.js');?>"></script>
        <script src="<?php echo base_url('assets/js/bidan_arum.js');?>"></script>

        <script>
            var ctx = $("#chartContainer")[0].getContext('2d');

            $.ajax({
                url: "<?= base_url() ?>chart/chartpemeriksaan",
                type: "POST",
                dataType: "JSON",
                success: function(data) {

                    var periksa = {
                        UmumLabel : [],
                        Umum : [],
                        Bidan : []
                    };

                    var len = data.length;

                    for (var i = 0; i < len; i++) {
                        if (data[i].pemeriksaan == "umum") {
                            periksa.UmumLabel.push(data[i].bulan);
                            periksa.Umum.push(data[i].jumlah);
                        } else if (data[i].pemeriksaan == "bidan") {
                            periksa.Bidan.push(data[i].jumlah);
                        }
                    }

                    var myChart = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: periksa.UmumLabel,
                            datasets: [{
                                label: 'Pemeriksaan Umum ',
                                data: periksa.Umum,
                                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                                borderColor: 'rgba(255,99,132,1)',
                                borderWidth: 1,
                                // fill: false,
                                lineTension: 0,
                                pointRadius: 3
                            }, {
                                label: 'Pemeriksaan Bidan ',
                                data: periksa.Bidan,
                                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                                borderColor: 'rgba(54, 162, 235, 1)',
                                borderWidth: 1,
                                // fill: false,
                                lineTension: 0,
                                pointRadius: 3
                            }]
                        },
                        options: {
                            legend: {
                                display: true,
                                position: "bottom"
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }],
                                xAxes: [{
                                    gridLines: {
                                        display: false
                                    }
                                }]
                            }
                        }
                    });
                }
            });
        </script>
    </body>
</html>