<?php $this->load->view('templates/header') ?>
  
    <div class="content-page">
    	<!-- Start content -->
        <div class="content">
            <div class="container">
                <!-- Start Widget -->
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-lg-4">
                        <div class="mini-stat clearfix bx-shadow">
                            <span class="mini-stat-icon bg-info"><i class="fa fa-bed"></i></span>
                            <div class="mini-stat-info text-right text-muted">
                                <span class="counter"><?= $pasien ?></span>
                                Total Pasien
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-lg-4">
                        <div class="mini-stat clearfix bx-shadow">
                            <span class="mini-stat-icon bg-danger"><i class="fa fa-stethoscope"></i></span>
                            <div class="mini-stat-info text-right text-muted">
                                <span class="counter"><?= $umum ?></span>
                                Pemeriksaan Umum
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-6 col-lg-4">
                        <div class="mini-stat clearfix bx-shadow">
                            <span class="mini-stat-icon bg-success"><i class="fa fa-user-md"></i></span>
                            <div class="mini-stat-info text-right text-muted">
                                <span class="counter"><?= $bidan ?></span>
                                Pemeriksaan Bidan
                            </div>
                        </div>
                    </div>
                </div> 
                <!-- End row-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title"><?= $headerChart ?></h1>
                    </div>
                    <div class="panel-body">
                        <canvas id="chartContainer" width="400" height="130"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('templates/footer') ?>