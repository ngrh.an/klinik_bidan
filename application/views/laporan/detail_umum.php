<?php $this->load->view('templates/header') ?>
	<div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h1 class="panel-title"><?= $title ?></h1>
							</div>
							<div class="col-lg-6 text-right">
								<a href="<?= base_url() ?>pemeriksaan/umum/edit/<?= $pemeriksaan[0]->id ?>" class="btn btn-primary m-r-5"><i class="fa fa-edit"></i> Edit</a>
								<a href="<?= base_url() ?>pemeriksaan/umum/hapus/<?= $pemeriksaan[0]->id ?>" class="btn btn-danger" onclick="return confirm('Data laporan pasien <?= $pemeriksaan[0]->nama ?> akan dihapus secara permanen, apa anda yakin?');"><i class="fa fa-trash"></i> Hapus</a>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered">
							<col width="180">
							<tr>
								<th>Nama</th>
								<td><?= $pemeriksaan[0]->nama ?></td>
							</tr>
							<tr>
								<th>Umur</th>
								<td><?= $pemeriksaan[0]->umur ?></td>
							</tr>
							<tr>
								<th>Alamat</th>
								<td><?= $pemeriksaan[0]->alamat ?></td>
							</tr>
							<tr>
								<th>Tanggal Periksa</th>
								<td><?= date_format(date_create($pemeriksaan[0]->tgl_periksa), 'd-m-Y') ?></td>
							</tr>
							<tr>
								<th>Subjective</th>
								<td><?= nl2br($pemeriksaan[0]->subjective) ?></td>
							</tr>
							<tr>
								<th>Objective</th>
								<td><?= nl2br($pemeriksaan[0]->objective) ?></td>
							</tr>
							<tr>
								<th>Diagnosa</th>
								<td><?= nl2br($pemeriksaan[0]->diagnosa) ?></td>
							</tr>
							<tr>
								<th>Terapi</th>
								<td><?= nl2br($pemeriksaan[0]->terapi) ?></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('templates/footer') ?>