<?php $this->load->view('templates/header') ?>
  
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h1 class="panel-title"><?= $title ?></h1>
							</div>
							<div class="col-lg-6 text-right">
								<button id="harian" class="btn btn-primary">Harian</button>
								<button id="bulanan" class="btn btn-secondary">Bulanan</button>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<h4 class="judul">Tampilkan Laporan Harian</h4>
						<form class="form-inline m-b-15" id="form-filter">
							<div class="input-group">
                                <input type="text" class="form-control" placeholder="yyyy-mm-dd" id="datepicker" autocomplete="off">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
							<div class="input-group">
                                <input type="text" class="form-control" placeholder="yyyy-mm" id="datepicker1" autocomplete="off">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                            <select class="form-control" id="kategori">
                            	<option value="">Pilih Kategori</option>
                                <option value="kb">KB</option>
                                <option value="anc">ANC</option>
                                <option value="gynekologi">GYNEKOLOGI</option>
                                <option value="intranatal">INTRANATAL</option>
                                <option value="bbl+imunisasi">BBL+IMUNISASI</option>
                                <option value="postnatal">POSTNATAL</option>
                            </select>
						    <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
						    <button type="button" id="btn-reset" class="btn btn-secondary">Reset</button>
                        </form>
						<div style="overflow-x: auto;" id="tabel-laporan-bidan">
							<table id="tableBidan" class="display nowrap table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>No</th>
						                <th>Nama</th>
						                <th>Umur</th>
						                <th>Alamat</th>
						                <th>Tanggal Periksa</th>
						                <th>Diagnosa</th>
						                <th>Terapi</th>
						                <th>Jenis Pemeriksaan</th>
						            </tr>
						        </thead>
						        <tbody>
						        </tbody>
						    </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('templates/footer') ?>