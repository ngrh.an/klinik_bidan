<?php $this->load->view('templates/header') ?>
  
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h1 class="panel-title"><?= $title ?></h1>
							</div>
							<div class="col-lg-6 text-right">
								<button id="harian" class="btn btn-primary">Harian</button>
								<button id="bulanan" class="btn btn-secondary">Bulanan</button>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<h4 class="judul">Tampilkan Laporan Harian</h4>
						<form class="form-inline m-b-15" id="form-filter">
							<div class="input-group">
                                <input type="text" class="form-control" placeholder="yyyy-mm-dd" id="datepicker" autocomplete="off">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
							<div class="input-group">
                                <input type="text" class="form-control" placeholder="yyyy-mm" id="datepicker1" autocomplete="off">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
						    <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
						    <button type="button" id="btn-reset" class="btn btn-secondary">Reset</button>
						    <div style="float: right;">
						    	<a href="<?= base_url() ?>cetak" target="_blank" class="btn btn-warning"><i class="fa fa-print"></i> Cetak </a>
						    </div>
                        </form>
						<div style="overflow-x: auto;">
							<table id="tableUmum" class="display nowrap table table-striped table-bordered" cellspacing="0" width="100%">
						        <thead>
						            <tr>
						                <th>No</th>
						                <th>Nama</th>
						                <th>Umur</th>
						                <th>Alamat</th>
						                <th>Tanggal Periksa</th>
						                <th>Diagnosa</th>
						                <th>Terapi</th>
						            </tr>
						        </thead>
						        <tbody>
						        </tbody>
						    </table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php $this->load->view('templates/footer') ?>