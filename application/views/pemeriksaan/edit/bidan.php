<?php $this->load->view('templates/header') ?>
 
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title"><?= $title ?></h1>
					</div>
					<div class="panel-body">
						<form action="" method="post">
						<input type="hidden" class="form-control" name="id" id="id" value="<?= $pemeriksaan[0]->id ?>" readonly>
						<input type="hidden" class="form-control" name="id_pasien" id="id_pasien" value="<?= $pemeriksaan[0]->id_pasien ?>" readonly>
						  <div class="form-group row">
						    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="nama" id="nama" value="<?= $pemeriksaan[0]->nama ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="umur" class="col-sm-3 col-form-label">Umur</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="umur" id="umur" value="<?= $pemeriksaan[0]->umur ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="alamat" id="alamat" value="<?= $pemeriksaan[0]->alamat ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="tanggal" class="col-sm-3 col-form-label">Tanggal Periksa</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?= date_format(date_create($pemeriksaan[0]->tgl_periksa), 'd M Y'); ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="subjektif" class="col-sm-3 col-form-label">Data Subjective</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="subjective" id="subjektif" placeholder="Masukkan data subjective" autocomplete="off" value="<?= $pemeriksaan[0]->subjective ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="objective" class="col-sm-3 col-form-label">Data Objective</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="objective" id="objective" placeholder="Masukkan data objective" autocomplete="off" value="<?= $pemeriksaan[0]->objective ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="diagnosa" class="col-sm-3 col-form-label">Diagnosa</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="diagnosa" id="diagnosa" placeholder="Masukkan diagnosa" autocomplete="off" value="<?= $pemeriksaan[0]->diagnosa ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="terapi" class="col-sm-3 col-form-label">Terapi</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="terapi" id="terapi" placeholder="Masukkan terapi" autocomplete="off" value="<?= $pemeriksaan[0]->terapi ?>">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="keterangan" class="col-sm-3 col-form-label">Keterangan</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Masukkan keterangan" autocomplete="off" value="<?= $pemeriksaan[0]->keterangan ?>">
						    </div>
						  </div>
							<div class="form-group row">
							    <small class="col-sm-3 form-text text-danger">Simpan sebagai laporan?</small>
							</div>
						  <button type="submit" name="laporan" value="anc" class="btn btn-primary">ANC</button>
						  <button type="submit" name="laporan" value="intranatal" class="btn btn-primary">INTRANATAL</button>
						  <button type="submit" name="laporan" value="postnatal" class="btn btn-primary">POSTNATAL</button>
						  <button type="submit" name="laporan" value="bbc+imunisasi" class="btn btn-primary">BBC+IMUNISASI</button>
						  <button type="submit" name="laporan" value="kb" class="btn btn-primary">KB</button>
						  <button type="submit" name="laporan" value="gynekologi" class="btn btn-primary">GYNEKOLOGI</button>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
    
<?php $this->load->view('templates/footer') ?>