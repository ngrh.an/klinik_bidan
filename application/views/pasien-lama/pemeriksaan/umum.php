<?php $this->load->view('templates/header') ?>
 
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
			<div class="container">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h1 class="panel-title"><?= $title ?></h1>
					</div>
					<div class="panel-body">
						<form action="" method="post">
						<input type="hidden" class="form-control" name="id_pasien" id="id" value="<?= $pemeriksaan[0]->id ?>" readonly>
						  <div class="form-group row">
						    <label for="nama" class="col-sm-3 col-form-label">Nama</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="nama" id="nama" value="<?= $pemeriksaan[0]->nama ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="umur" class="col-sm-3 col-form-label">Umur</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="umur" id="umur" value="<?= $umur ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="alamat" id="alamat" value="<?= $pemeriksaan[0]->alamat ?>" readonly>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="tanggal" class="col-sm-3 col-form-label">Tanggal Periksa</label>
						    <div class="col-sm-9">
						      <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?= $tanggal = set_value('tanggal') == false ? set_value('tanggal') : ''; ?>" placeholder="dd-mm-yyyy" autocomplete="off">
						      <small class="form-text text-danger"><?= form_error('tanggal') ?></small>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="subjektif" class="col-sm-3 col-form-label">Data Subjective</label>
						    <div class="col-sm-9">
						      <textarea type="text" class="form-control" name="subjective" id="subjektif" placeholder="Masukkan data subjective" autocomplete="off"></textarea>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="objective" class="col-sm-3 col-form-label">Data Objective</label>
						    <div class="col-sm-9">
						      <textarea type="text" class="form-control" name="objective" id="objective" placeholder="Masukkan data objective" autocomplete="off"></textarea>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="diagnosa" class="col-sm-3 col-form-label">Diagnosa</label>
						    <div class="col-sm-9">
						      <textarea type="text" class="form-control" name="diagnosa" id="diagnosa" placeholder="Masukkan diagnosa" autocomplete="off"></textarea>
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="terapi" class="col-sm-3 col-form-label">Terapi</label>
						    <div class="col-sm-9">
						      <textarea type="text" class="form-control" name="terapi" id="terapi" placeholder="Masukkan terapi" autocomplete="off"></textarea>
						    </div>
						  </div>
						  <button type="submit" class="btn btn-primary simpan">Simpan</button>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
    
<?php $this->load->view('templates/footer') ?>